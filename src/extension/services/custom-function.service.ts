import { CustomFunctionDto } from '@cheetah/dtos/extension/custom-function.dto';
import { Injectable } from '@nestjs/common';
import { CustomFunctionRepository } from '../repositories/custom-function.repository';

@Injectable()
export class CustomFunctionService {
  constructor(
    private readonly customFunctionRepository: CustomFunctionRepository,
  ) {}

  async newCustomFunction(options: { filePath: string; companyId: string }) {
    await this.customFunctionRepository.insertOne({
      filePath: options.filePath,
      companyId: options.companyId,
      isInReview: true,
      isVerified: false,
      isPublic: false,
      status: false,
    });
  }

  async customFunctionsList(
    companyId: CustomFunctionDto['companyId'],
  ): Promise<CustomFunctionDto[]> {
    return await this.customFunctionRepository.findMany({ companyId });
  }
}
